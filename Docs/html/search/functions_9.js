var searchData=
[
  ['miparams',['MIParams',['../structMIParams.html#aacca20277bf120fca95f3641018a6556',1,'MIParams::MIParams(const AMParams *am_params, int _n_bins, double _pre_seed, bool _partition_of_unity, double _likelihood_alpha, ImageBase *_pix_mapper, bool _debug_mode)'],['../structMIParams.html#a7d61e0e505a06adcc6631c70ea115735',1,'MIParams::MIParams(const MIParams *params=nullptr)']]],
  ['mtfnet',['MTFNet',['../classutils_1_1MTFNet.html#a2fae5241e9c54156b6e6a880671e0e08',1,'utils::MTFNet']]]
];
