var searchData=
[
  ['kld',['KLD',['../classKLD.html',1,'']]],
  ['kldparams',['KLDParams',['../structKLDParams.html',1,'KLDParams'],['../structKLDParams.html#a1a9a0256a98714fcfe8bdc691bce2ce1',1,'KLDParams::KLDParams(const AMParams *am_params, int _n_bins, double _pre_seed, bool _partition_of_unity, bool _debug_mode)'],['../structKLDParams.html#ab8912d7579d8b221ad839b31f7f19e29',1,'KLDParams::KLDParams(const KLDParams *params=nullptr)']]]
];
