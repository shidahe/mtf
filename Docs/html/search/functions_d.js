var searchData=
[
  ['riuparams',['RIUParams',['../structRIUParams.html#a16ded8068839c9509e20d3af5ae99ac0',1,'RIUParams::RIUParams(const AMParams *am_params, double _likelihood_alpha, bool _debug_mode)'],['../structRIUParams.html#a9bd097e9e1ec26aa669799042338484e',1,'RIUParams::RIUParams(const RIUParams *params=nullptr)']]],
  ['rscvparams',['RSCVParams',['../structRSCVParams.html#aa0d3faa70b8088967950256bb7d1d1c0',1,'RSCVParams::RSCVParams(const AMParams *am_params, bool _use_bspl, int _n_bins, double _pre_seed, bool _partition_of_unity, bool _weighted_mapping, bool _mapped_gradient, bool _approx_dist_feat, bool _debug_mode)'],['../structRSCVParams.html#addd6cd24344dfc1383ab659943bf0691',1,'RSCVParams::RSCVParams(const RSCVParams *params=nullptr)']]]
];
