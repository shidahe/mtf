var searchData=
[
  ['len_5fhistory',['len_history',['../structPCAParams.html#a17b133133e534514d088b902d2ed31f4',1,'PCAParams']]],
  ['likelihood_5falpha',['likelihood_alpha',['../structCCREParams.html#a554cb54ca0c4c707476e70d5c4a0f7d5',1,'CCREParams::likelihood_alpha()'],['../structMIParams.html#a6078e752c2e4a5836030a5a29bf77d77',1,'MIParams::likelihood_alpha()'],['../structNCCParams.html#acd8c9b4bbdac61aec95c22ee1483bc15',1,'NCCParams::likelihood_alpha()'],['../structRIUParams.html#ae015e72075a4e62df51e7064fedcf5f3',1,'RIUParams::likelihood_alpha()'],['../structSPSSParams.html#a9f47f5c1d11aa0f89d366083da7df816',1,'SPSSParams::likelihood_alpha()'],['../structSSIMParams.html#ac66697e2f8738395fa415c6d3f726774',1,'SSIMParams::likelihood_alpha()'],['../structZNCCParams.html#aecf018afbe10fb4062ea0a0993bfb127',1,'ZNCCParams::likelihood_alpha()']]]
];
